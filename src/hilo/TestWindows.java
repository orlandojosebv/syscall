/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hilo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 *
 * @author docente
 */
public class TestWindows {
    
    public static void main(String nada[]) throws FileNotFoundException, IOException {
        SysCall mySyscall=new SysCall();
        String comando2[]={"cmd.exe","/c","echo 'nada'"};
        String comando3[]={"cmd.exe","/c","dir"};
        String comando4[]={"cmd.exe","/c","notepad.exe"};
        mySyscall.run(comando4);
        mySyscall.printer();
        String msg="Welcome O.S :)";
        System.out.println(msg);
        //Cambiar la salida standar a un archivo:
        FileOutputStream salida=new FileOutputStream("src/datos/salida.txt");
        File fileName=new File("src/datos/salida.txt");
        //Cambia la salida standar del sistema:
        PrintStream inicial=System.out; //almacenan la salida vieja
        System.setOut(new PrintStream(salida));
        System.out.println(msg);
        System.setOut(inicial);
        //cerrar el archivo:
        salida.close();
             
        
        
    }
    
}
